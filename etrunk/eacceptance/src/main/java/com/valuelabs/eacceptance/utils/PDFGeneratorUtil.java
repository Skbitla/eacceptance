package com.valuelabs.eacceptance.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.valuelabs.eacceptance.model.EmployeeEAcceptance;


@Component
public class PDFGeneratorUtil {

    @Autowired
    private Environment environment;

    private static final Logger logger = Logger.getLogger(PDFGeneratorUtil.class);

    private static String userPdfHtml;
    private static String hrPdfHtml;

    private static String fromAddress ;
    private static String folderPath;
    private static String hrEmail;
    private static String SMTP_HOST ;
    private static String SMTP_PORT ;
    private static String MAIL_USERNAME ;
    private static String MAIL_PASSWORD ;
    private static String applicationUrl;

    public void updateUserAndHR(EmployeeEAcceptance employeeEAcceptance) {
        String userSubject = "RE: Change in exit clause - Digitally accepted copy";
        String userMessage = "";
        String hrSubject = "RE: Change in exit clause - Digitally accepted copy" + employeeEAcceptance.getEmployeeId();
        String hrMessage = "";
        if (employeeEAcceptance.isAcceptance() != null && employeeEAcceptance.isAcceptance()) {
            userMessage = "Dear " + employeeEAcceptance.getEmployeeFirstName() + " " +employeeEAcceptance.getEmployeeLastName()+", </br> </br>";
            userMessage += "<html>"+
"<body>"+
"<p><span style=\"color: #203864;\">Thank You for accepting the amendment in your exit clause.</span></p>"+
"<p><span style=\"color: #203864;\">Please find attached a copy of the same, for your perusal.</span></p>"+
"<p><span style=\"color: #203864;\">&nbsp;</span></p>"+
"<p><span style=\"font-size: 10pt; font-family: 'arial','sans-serif'; color: #203864;\">Best regards,</span></p>"+
"<p><strong><span style=\"font-size: 10pt; font-family: 'arial','sans-serif'; color: #203864;\">HR@ValueLabs</span></strong></p>"+
"<p><span style=\"font-size: 10pt; font-family: 'arial','sans-serif'; color: #203864;\">ValueLabs &ndash; &lsquo;<em>inspired by potential&rsquo;</em></span></p>"+
"<p>&nbsp;</p>"+
"<p>&nbsp;</p>"+
"<p>&nbsp;</p>"+
"</body>"+
"</html>";
            hrMessage = employeeEAcceptance.getEmployeeId() + " " + employeeEAcceptance.getEmployeeFirstName() + " " + employeeEAcceptance.getEmployeeLastName() + "  Accepted the Notice Period Clause";
        }

        String userFileName = employeeEAcceptance.getEmployeeId() + "NoticePeriod Update.pdf";
        String hRFileName = employeeEAcceptance.getEmployeeId() + "HR NoticePeriod Update.pdf";
        //first send to user
        createPdfAndSendEmail(employeeEAcceptance.getEmployeeEmailID(), userSubject, userMessage, true, employeeEAcceptance, userFileName);//for user

        //send to hr
        createPdfAndSendEmail(hrEmail, hrSubject, hrMessage, false, employeeEAcceptance, hRFileName);//for HR

    }

    private void createPdfAndSendEmail(String userEmail, String subject,
                                       String message, boolean forUser,
                                       EmployeeEAcceptance employeeEAcceptance, String fileName) {
//		if (!employeeEAcceptance.isAcceptance()) {
//			sendEmail(null, userEmail, null, subject, message);
//			return;
//		}

        String input1 = null;
        SimpleDateFormat format = new SimpleDateFormat("dd MMM, yyyy");
        String createdDate = format.format(new Date());
        String joiningDate = format.format(employeeEAcceptance.getEmployeeDOJ());

        if (forUser) {
            if (userPdfHtml == null || userPdfHtml.equals("")) {
                userPdfHtml = getFileContents("user.htm");
            }
            input1 = userPdfHtml.toString();
        } else {
            if (hrPdfHtml == null || hrPdfHtml.equals("")) {
                hrPdfHtml = getFileContents("hradmin.htm");
            }
            input1 = hrPdfHtml.toString();
        }
        input1 = input1.replaceAll("<<<EMP_EMAIL_ID>>>",
                employeeEAcceptance.getEmployeeEmailID());
        input1 = input1.replaceAll("<<<CREATED_DT>>>", createdDate);
        input1 = input1.replaceAll("<<<DOJ_DT>>>", joiningDate);
        input1 = input1.replaceAll("<<<EMPLOYEENAME>>>",
                employeeEAcceptance.getEmployeeFirstName() + " "
                        + employeeEAcceptance.getEmployeeLastName());
        input1 = input1.replaceAll("<<<EMP_ID>>>", employeeEAcceptance.getEmployeeId());
        input1 = input1.replaceAll("<<<EMP_ENITITY>>>",
                employeeEAcceptance.getEmployeeEntity());

        OutputStream fileOutputStream = null;
        try {
            File userFile = new File(folderPath + fileName);
            if (userFile.exists()){
                userFile.delete();
                userFile.createNewFile();
            }

            fileOutputStream = new FileOutputStream(userFile);
            Document document = new Document();
            PdfWriter pdfWriter = PdfWriter.getInstance(document,
                    fileOutputStream);
            document.open();
            XMLWorkerHelper.getInstance().parseXHtml(pdfWriter, document, new StringReader(input1));
            Image img = Image.getInstance(PDFGeneratorUtil.class.getClassLoader().getResource("valuelabsimg.png"));
            img.setAbsolutePosition(0, PageSize.A4.getHeight() - img.getScaledHeight());
            document.add(img);
            document.close();
            fileOutputStream.close();
            sendEmail(userFile.getPath(), userEmail, userFile.getName(),subject, message);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {

                }
            }
        }
    }

    private String getFileContents(String fileName) {

        StringBuilder result = new StringBuilder("");

        // Get file from resources folder
        ClassLoader classLoader = PDFGeneratorUtil.class.getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());
        Scanner scanner = null;
        try {
        	scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                result.append(line).append("\n");
            }

            scanner.close();

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
        	if(scanner != null){
        		scanner.close();
        	}
        }

        return result.toString();
    }

    private boolean sendEmail(String attachementPath, String mailTo,
                              String fileName, String subject, String message) {
        Properties props = new java.util.Properties();
        props.put("mail.smtp.host", SMTP_HOST);
//		props.put("mail.smtp.host", S);
        props.put("mail.smtp.port", SMTP_PORT);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        // Session session = Session.getDefaultInstance(props, null);
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(MAIL_USERNAME, MAIL_PASSWORD);
                    }
                });

        // Session session = getInstance(props,null);
        Message msg = new MimeMessage(session);
        try {

            msg.setFrom(new InternetAddress(fromAddress));
            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(mailTo));
            msg.setSubject(subject);

            Multipart multipart = new MimeMultipart();

            MimeBodyPart textBodyPart = new MimeBodyPart();
            textBodyPart.setContent(message, "text/html; charset=utf-8");

            if (attachementPath != null && fileName != null) {
                MimeBodyPart attachmentBodyPart = new MimeBodyPart();
                DataSource source = new FileDataSource(attachementPath);
                attachmentBodyPart.setDataHandler(new DataHandler(source));
                attachmentBodyPart.setFileName(fileName);

                multipart.addBodyPart(attachmentBodyPart);
            }

            multipart.addBodyPart(textBodyPart);


            msg.setContent(multipart);
            Transport.send(msg);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        return true;
    }

    public boolean sendMutipleEmail(String subject, List<EmployeeEAcceptance> reciepents) {
        Properties props = new java.util.Properties();
        props.put("mail.smtp.host", SMTP_HOST);
//		props.put("mail.smtp.host", S);
        props.put("mail.smtp.port", SMTP_PORT);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        
        // Session session = Session.getDefaultInstance(props, null);
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(MAIL_USERNAME, MAIL_PASSWORD);
                    }
                });

        // Session session = getInstance(props,null);
        Transport transport = null;
        try {
            transport = session.getTransport("smtp");
            transport.connect();
            String finalMessage = "<html>" + 
"<body>"+
"<p>As you are aware, there has been a change in the exit clause.</p>"+
"<p>The details are as per the communication shared on August 4<sup>th</sup>,2017.</p>"+
"<p>Please treat this as a reminder to accept the same, at the earliest.</p>"+
"<p>Kindly click on the link below to provide you<span style=\"color: #1f497d;\">r</span> acceptance</p>"+
"<p>&nbsp;</p>"+
"<a href=\""+applicationUrl+"\"><h4>exitpolicy.valuelabs.com</h4></a>"+
"<p>&nbsp;</p>"+
"<p><span style=\"font-size: 10pt; font-family: 'arial','sans-serif'; color: black;\">Best regards,</span></p>"+
"<p><strong><span style=\"font-size: 10pt; font-family: 'arial','sans-serif'; color: #1f497d;\">HR@ValueLabs</span></strong></p>"+
"<p><span style=\"font-size: 10pt; font-family: 'arial','sans-serif'; color: black;\">ValueLabs &ndash; &lsquo;<em>inspired by potential&rsquo;</em></span></p>"+
"<p>&nbsp;</p>"+
"</body>"+
"</html>";
            for (EmployeeEAcceptance employeeEAcceptance : reciepents) {
              String  message = "Dear " + employeeEAcceptance.getEmployeeFirstName() + " " +employeeEAcceptance.getEmployeeLastName()+", </br> </br>" + finalMessage; 
                Message msg = new MimeMessage(session);
                msg.setFrom(new InternetAddress(fromAddress));
                InternetAddress[] address = {new InternetAddress(employeeEAcceptance.getEmployeeEmailID())};
                msg.setSubject(subject);

                Multipart multipart = new MimeMultipart();

                MimeBodyPart textBodyPart = new MimeBodyPart();
                textBodyPart.setContent(message, "text/html; charset=utf-8");

                multipart.addBodyPart(textBodyPart);

                msg.setContent(multipart);
                transport.sendMessage(msg, address);

            }
        } catch (MessagingException e1) {
            logger.log(Priority.ERROR, "Failed sending multiple emails");
        }

        return true;
    }

    public File createAcceptancePdf(EmployeeEAcceptance employeeEAcceptance, String fileName) {

        String input1 = null;
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        String createdDate = format.format(employeeEAcceptance.getCreateDate());
        String joiningDate = format.format(employeeEAcceptance.getEmployeeDOJ());

        if (hrPdfHtml == null || hrPdfHtml.equals("")) {
            hrPdfHtml = getFileContents("hradmin.htm");
        }
        input1 = hrPdfHtml.toString();
        input1 = input1.replaceAll("<<<EMP_EMAIL_ID>>>",
                employeeEAcceptance.getEmployeeEmailID());
        input1 = input1.replaceAll("<<<CREATED_DT>>>", createdDate);
        input1 = input1.replaceAll("<<<DOJ_DT>>>", joiningDate);
        input1 = input1.replaceAll("<<<EMPLOYEENAME>>>",
                employeeEAcceptance.getEmployeeFirstName() + " "
                        + employeeEAcceptance.getEmployeeLastName());
        input1 = input1.replaceAll("<<<EMP_ID>>>", employeeEAcceptance.getEmployeeId());
        input1 = input1.replaceAll("<<<EMP_ENITITY>>>",
                employeeEAcceptance.getEmployeeEntity());
        File userFile = new File(folderPath + fileName);
        Document document = null;
        OutputStream fileOutputStream = null;
        try {
        	fileOutputStream = new FileOutputStream(userFile);
            document = new Document();
            PdfWriter pdfWriter = PdfWriter.getInstance(document,
                    fileOutputStream);
            document.open();
            XMLWorkerHelper.getInstance().parseXHtml(pdfWriter, document,
                    new StringReader(input1));
            
            Image img = Image.getInstance(PDFGeneratorUtil.class.getClassLoader().getResource("valuelabsimg.png"));
            img.setAbsolutePosition(0, PageSize.A4.getHeight() - img.getScaledHeight());
            document.add(img);
            
            document.close();
            fileOutputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }finally{
        	if(fileOutputStream!= null){
        		try {
					fileOutputStream.close();
				} catch (IOException e) {
					 
				}
        	}
        	if(document!= null){
        		document.close();
        		
        	}
        }

        return userFile;

    }

    @Value("${application.from.email}")
    public void setFromAddress(String fromAddress) {
        PDFGeneratorUtil.fromAddress = fromAddress;
    }

    @Value("${pdf.folderPath}")
    public void setFolderPath(String folderPath) {
        PDFGeneratorUtil.folderPath = folderPath;
    }

    @Value("${hr.email}")
    public void setHrEmail(String hrEmail) {
        PDFGeneratorUtil.hrEmail = hrEmail;
    }

    @Value("${email.smtp.host}")
    public void setSmtpHost(String smtpHost) {
        SMTP_HOST = smtpHost;
    }

    @Value("${email.smtp.port}")
    public void setSmtpPort(String smtpPort) {
        SMTP_PORT = smtpPort;
    }

    @Value("${email.smtp.username}")
    public void setMailUsername(String mailUsername) {
        MAIL_USERNAME = mailUsername;
    }

    @Value("${email.smtp.password}")
    public void setMailPassword(String mailPassword) {
        MAIL_PASSWORD = mailPassword;
    }

    @Value("${applicationURL}")
	public void setApplicationUrl(String applicationUrl) {
		PDFGeneratorUtil.applicationUrl = applicationUrl;
	}
    
    

}

