package com.valuelabs.eacceptance.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;


public class CVSUtilExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			String folderPath = "E:\\csv";
			File file = new File(folderPath + "sample.csv");
			if(file.exists()){
				file.delete();
				file.createNewFile();
			}
			
			FileWriter writer = new FileWriter(file);
			String[] projects = {"Benefitfocus","JDA","CYINET","INFOSYS","HCL"};
			MkYongCSVUtils.writeLine(writer, Arrays.asList("Project", "Thesis", "JANUARY", "FEBUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"));
			
			
			for(String project : projects){
				// custom separator + quote
				MkYongCSVUtils.writeLine(writer, Arrays.asList(project, "Accepted", ","," ,,","march","aprilData","MayData"), ',', '"');
				MkYongCSVUtils.writeLine(writer, Arrays.asList("", "Done", ""), ',', '"');
				MkYongCSVUtils.writeLine(writer, Arrays.asList("", "Not completed", ""), ',', '"');
				MkYongCSVUtils.writeLine(writer, Arrays.asList("", "Yet to done", ""), ',', '"');
				MkYongCSVUtils.writeLine(writer, Arrays.asList("", "Review", ""), ',', '"');
			}



/*			// custom separator + quote
			MkYongCSVUtils.writeLine(writer, Arrays.asList("aaa", "bbb", "cc,c"), '|', '\'');
			MkYongCSVUtils.writeLine(writer, Arrays.asList("aaa", "bbb", "cc\"c"));*/
			
			System.out.println("File created");
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}