package com.valuelabs.eacceptance.controller;

import com.valuelabs.eacceptance.model.EAcceptacneListPaginationBean;
import com.valuelabs.eacceptance.model.EmployeeAcceptanceCount;
import com.valuelabs.eacceptance.model.EmployeeEAcceptance;
import com.valuelabs.eacceptance.service.EAcceptanceService;
import com.valuelabs.eacceptance.utils.CSVUtil;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
public class EAcceptanceController {

    @Autowired
    EAcceptanceService eAcceptanceService;
    
    @Autowired
    CSVUtil csvUtil;
    
    private String jwtKey;

    @Value("${jwt.secretKey}")
    public void setJwtKey(String jwtKey) {
		this.jwtKey = jwtKey;
	}

	private static final String ADMIN = "admin";


    @RequestMapping(value = "/eAccept/{employeeUserName:.+}", method = RequestMethod.GET, headers = "Accept=application/json")
    public EmployeeEAcceptance getEmployeeAcceptance(HttpServletRequest httpServletRequest,
                                                     @PathVariable String employeeUserName) throws UnsupportedEncodingException {
        validateUser(httpServletRequest, null);
        return eAcceptanceService.getEmployeeEAcceptance(employeeUserName);
    }

    @RequestMapping(value = "/eAccept/update/{userName:.+}/{acceptance}", method = RequestMethod.GET, headers = "Accept=application/json")
    public void setEmployeeAcceptance(HttpServletRequest httpServletRequest, @PathVariable String userName,
                                      @PathVariable String acceptance) throws Exception {
        validateUser(httpServletRequest, null);
        boolean eacceptance = "true".equalsIgnoreCase(acceptance);
        eAcceptanceService.addOrUpdateEmployeeEAcceptance(userName, eacceptance);

    }

    @RequestMapping(value = "/eAccept/counts", method = RequestMethod.GET, headers = "Accept=application/json")
    public EmployeeAcceptanceCount getEmployeeAcceptanceCount(HttpServletRequest httpServletRequest) throws UnsupportedEncodingException {
        validateUser(httpServletRequest, ADMIN);
        return eAcceptanceService.getEmployeeAcceptanceCount();
    }

    @RequestMapping(value = "/eAccept/sendEmailToNotAccepted", method = RequestMethod.GET, headers = "Accept=application/json")
    public void sendMultipleEMail(HttpServletRequest httpServletRequest) throws UnsupportedEncodingException {
        validateUser(httpServletRequest, ADMIN);
        //eAcceptanceService.sendEmailToNoActionReceiptents();
    }

    @RequestMapping(value = "/eAccept/paginatedList/{accepted}", method = RequestMethod.GET, headers = "Accept=application/json")
    public EAcceptacneListPaginationBean getEmployeesListByType(HttpServletRequest httpServletRequest, @PathVariable String accepted, @RequestParam("pageNumber") int pageNumber, @RequestParam("pageSize") int pageSize) throws UnsupportedEncodingException {
        validateUser(httpServletRequest, ADMIN);
        Boolean acceptance;
        if ("true".equalsIgnoreCase(accepted)) {
            acceptance = Boolean.TRUE;
        } else {
            acceptance = Boolean.FALSE;
        }

        return eAcceptanceService.getEmployeeListByAcceptanceType(acceptance, pageNumber, pageSize);
    }

    @RequestMapping(value = "/eAccept/list/{accepted}", method = RequestMethod.GET, headers = "Accept=application/json")
    public List<EmployeeEAcceptance> getAllEmployeesListByType(HttpServletRequest httpServletRequest, @PathVariable String accepted) throws UnsupportedEncodingException {

        validateUser(httpServletRequest, ADMIN);
        Boolean acceptance;
        if ("true".equalsIgnoreCase(accepted)) {
            acceptance = Boolean.TRUE;
        } else {
            acceptance = Boolean.FALSE;
        }

        return eAcceptanceService.getAllEmployeeListByAcceptanceType(acceptance);
    }

    @RequestMapping(value = "/eAccept/download/{employeeUserName:.+}", method = RequestMethod.GET, produces = "application/pdf")
    public @ResponseBody
    HttpEntity<byte[]> downloadAcceptanceLetter(HttpServletRequest httpServletRequest, @PathVariable String employeeUserName) throws IOException {
        File file = eAcceptanceService.getAcceptancePdf(employeeUserName);
        byte[] document = FileCopyUtils.copyToByteArray(file);

        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "pdf"));
        header.set("Content-Disposition", "inline; filename=" + file.getName());
        header.setContentLength(document.length);

        return new HttpEntity<byte[]>(document, header);
    }

    
    @RequestMapping(value = "/eAccept/downloadReport/{acceptanceType}", method = RequestMethod.GET, produces = "text/csv")
    public @ResponseBody
    HttpEntity<byte[]> downloadReport(HttpServletRequest httpServletRequest, @PathVariable String acceptanceType) throws IOException {
        
        Boolean acceptance;
        if ("true".equalsIgnoreCase(acceptanceType)) {
            acceptance = Boolean.TRUE;
        } else {
            acceptance = Boolean.FALSE;
        }
        File file = csvUtil.createReport(acceptance);
        byte[] document = FileCopyUtils.copyToByteArray(file);

        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("text", "csv"));
        header.set("Content-Disposition", "inline; filename=" + file.getName());
        header.setContentLength(document.length);

        return new HttpEntity<byte[]>(document, header);
    }
    
    public void validateUser(HttpServletRequest httpServletRequest, String role) throws UnsupportedEncodingException {
        try{
            String authorization = httpServletRequest.getHeader("Authorization");

            Jws<Claims> claims = Jwts.parser()
                    .setSigningKey(jwtKey)
                    .parseClaimsJws(authorization);
            String scope = (String) claims.getBody().get("scope");
            if (role != null) {
                if (!scope.equals(role)) {
                    throw new IllegalStateException("invalid token");
                }
            }
        }catch (Exception e){
            throw new IllegalStateException("token is not valid");
        }



    }

}
