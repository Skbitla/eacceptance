package com.valuelabs.eacceptance.model;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

@Entity
@Table(name = "eacceptance")
public class EmployeeEAcceptance {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;

	@Column(name = "EACCEPTANCE_IND")
	Boolean acceptance;

	@Column(name = "EMP_ID")
	String employeeId;
	
	@Column(name = "FIRST_NM")
	String employeeFirstName;
	
	@Column(name = "MIDDLE_NM")
	String employeeMiddleName;
	
	@Column(name = "LAST_NM")
	String employeeLastName;
	
	@Column(name = "EMP_USERNAME")
	String employeeUserName;
	
	@Column(name = "EMP_EMAIL_ID")
	String employeeEmailID;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MMM-yyyy", timezone = "IST") 
	@Column(name = "EMP_DOJ")
	Date employeeDOJ;
	
	@Column(name = "EMP_ENITITY")
	String employeeEntity;
	
	@Column(name = "EMP_LOCATION")
	String employeeGeoLocation;
	
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MMM-yyyy", timezone = "IST")
	@Column(name = "CREATED_DT")
	Date createDate;

	public EmployeeEAcceptance() {
	}

	public EmployeeEAcceptance(Integer id, String employeeID, boolean acceptance, String employeeFirstName, String employeeMiddleName, String employeeUserName,
							   String employeeEmailID, Date employeeDOJ, String employeeEntity, String employeeGeoLocation, Date createDate) {
		super();
		this.id = id;
		this.employeeId = employeeID;
		this.acceptance = acceptance;
		this.employeeFirstName = employeeFirstName;
		this.employeeMiddleName= employeeMiddleName; 
		this.employeeUserName = employeeUserName;
		this.employeeEmailID = employeeEmailID;
		this.employeeDOJ = employeeDOJ;
		this.employeeEntity = employeeEntity;
		this.employeeGeoLocation = employeeGeoLocation;
		this.createDate = createDate;
	}

	public String getEmployeeFirstName() {
		return employeeFirstName;
	}

	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeMiddleName() {
		return employeeMiddleName;
	}

	public void setEmployeeMiddleName(String employeeMiddleName) {
		this.employeeMiddleName = employeeMiddleName;
	}

	public String getEmployeeLastName() {
		return employeeLastName;
	}

	public void setEmployeeLastName(String employeeLastName) {
		this.employeeLastName = employeeLastName;
	}

	public String getEmployeeUserName() {
		return employeeUserName;
	}

	public void setEmployeeUserName(String employeeUserName) {
		this.employeeUserName = employeeUserName;
	}

	public String getEmployeeEmailID() {
		return employeeEmailID;
	}

	public void setEmployeeEmailID(String employeeEmailID) {
		this.employeeEmailID = employeeEmailID;
	}

	public Date getEmployeeDOJ() {
		return employeeDOJ;
	}

	public void setEmployeeDOJ(Date employeeDOJ) {
		this.employeeDOJ = employeeDOJ;
	}

	public String getEmployeeEntity() {
		return employeeEntity;
	}

	public void setEmployeeEntity(String employeeEntity) {
		this.employeeEntity = employeeEntity;
	}

	public String getEmployeeGeoLocation() {
		return employeeGeoLocation;
	}

	public void setEmployeeGeoLocation(String employeeGeoLocation) {
		this.employeeGeoLocation = employeeGeoLocation;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Boolean isAcceptance() {
		return acceptance;
	}

	public void setAcceptance(Boolean acceptance) {
		this.acceptance = acceptance;
	}

}
