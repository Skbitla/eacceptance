package com.valuelabs.eacceptance.service;

import com.valuelabs.eacceptance.dao.EAcceptanceDAO;
import com.valuelabs.eacceptance.model.EAcceptacneListPaginationBean;
import com.valuelabs.eacceptance.model.EmployeeAcceptanceCount;
import com.valuelabs.eacceptance.model.EmployeeEAcceptance;
import com.valuelabs.eacceptance.utils.PDFGeneratorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;

@Service("eAcceptanceService")
//@PropertySource("classpath:application.properties")
public class EAcceptanceService {

    @Autowired
    EAcceptanceDAO eAcceptanceDao;

    @Autowired
    PDFGeneratorUtil pdfGeneratorUtil;


    private static String NoActionSubject = "Reminder : Acceptance - Change in exit clause";

    @Transactional
    public EmployeeEAcceptance getEmployeeEAcceptance(String employeeUserName) {
        return eAcceptanceDao.getEmployeeEAcceptanceByUserName(employeeUserName);
    }

    @Transactional
    public void addOrUpdateEmployeeEAcceptance(String userName,
                                               boolean eAcceptance) throws Exception {
        EmployeeEAcceptance employeeEAcceptance = eAcceptanceDao
                .addOrUpdateEmployeeEAcceptance(userName, eAcceptance);

        if (employeeEAcceptance != null) {
            pdfGeneratorUtil.updateUserAndHR(employeeEAcceptance);
        }

    }

    public void sendEmailToNoActionReceiptents() {
        String sub = null;
        List<EmployeeEAcceptance> reciepents = eAcceptanceDao.getEmployeesByAcceptance(false);
        sub = NoActionSubject;
        if (reciepents != null && !reciepents.isEmpty()) {
            pdfGeneratorUtil.sendMutipleEmail(sub, reciepents);
        }

    }

    public EmployeeAcceptanceCount getEmployeeAcceptanceCount() {
        Long acceptedCounts = eAcceptanceDao.getEmployeesCountByAcceptance(true);
        Long noActionCounts = eAcceptanceDao.getEmployeesCountByAcceptance(false);

        EmployeeAcceptanceCount employeeAcceptanceCount = new EmployeeAcceptanceCount();
        //employeeAcceptanceCount.setAcceptedEmployeesCount(acceptedCounts);
        //employeeAcceptanceCount.setNoActionEmployeesCount(noActionCounts);

        return employeeAcceptanceCount;
    }

    public EAcceptacneListPaginationBean getEmployeeListByAcceptanceType(Boolean acceptance, int pageNumber, int pageSize) {

        EAcceptacneListPaginationBean acceptacneListPaginationBean = new EAcceptacneListPaginationBean();
        List<EmployeeEAcceptance> employees = eAcceptanceDao.getEmployeesByAcceptanceType(acceptance, pageNumber, pageSize);
        acceptacneListPaginationBean.setList(employees);


        return acceptacneListPaginationBean;
    }

    public List<EmployeeEAcceptance> getAllEmployeeListByAcceptanceType(Boolean acceptance) {
        List<EmployeeEAcceptance> employees = eAcceptanceDao.getEmployeesByAcceptance(acceptance);

        return employees;
    }

    public File getAcceptancePdf(String employeeUserName) {
    	System.out.println("username "+ employeeUserName);

        EmployeeEAcceptance employeeEAcceptance = eAcceptanceDao
                .getEmployeeEAcceptanceByUserName(employeeUserName);
        if(employeeEAcceptance != null){
            String hRFileName = employeeEAcceptance.getEmployeeId() + "HR NoticePeriod Update.pdf";
            return  pdfGeneratorUtil.createAcceptancePdf(employeeEAcceptance, hRFileName);        	
        }
return null;
    }
}
