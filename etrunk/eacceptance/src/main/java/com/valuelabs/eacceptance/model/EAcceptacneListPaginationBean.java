package com.valuelabs.eacceptance.model;

import java.util.List;

public class EAcceptacneListPaginationBean {

	private List<EmployeeEAcceptance> employees = null;

	private boolean last;
	private int totalEmployees;
	private int totalPages;
	private int currentPage;
	private int employeesPerPage;
	private boolean first;

	public List<EmployeeEAcceptance> getList() {
		return employees;
	}

	public void setList(List<EmployeeEAcceptance> list) {
		this.employees = list;
	}

	public boolean isLast() {
		return last;
	}

	public void setLast(boolean last) {
		this.last = last;
	}

	public int getTotalEmployees() {
		return totalEmployees;
	}

	public void setTotalEmployees(int totalEmployees) {
		this.totalEmployees = totalEmployees;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getEmployeesPerPage() {
		return employeesPerPage;
	}

	public void setEmployeesPerPage(int employeesPerPage) {
		this.employeesPerPage = employeesPerPage;
	}

	public boolean isFirst() {
		return first;
	}

	public void setFirst(boolean first) {
		this.first = first;
	}

}
