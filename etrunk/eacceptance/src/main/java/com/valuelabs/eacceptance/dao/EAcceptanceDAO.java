package com.valuelabs.eacceptance.dao;

import com.valuelabs.eacceptance.model.EmployeeEAcceptance;
import org.hibernate.*;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class EAcceptanceDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }

    public EmployeeEAcceptance addOrUpdateEmployeeEAcceptance(String userName,
                                                              boolean acceptance) throws Exception {
        EmployeeEAcceptance employeeEAcceptance = getEmployeeEAcceptanceByUserName(userName);
        if (employeeEAcceptance == null) {
            return null;
        }
        employeeEAcceptance.setAcceptance(acceptance);
        employeeEAcceptance.setCreateDate(new Date());

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(employeeEAcceptance);
            tx.commit();
        } catch (Exception e) {
            if (tx != null)
                tx.rollback();
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return employeeEAcceptance;
    }

    public List<EmployeeEAcceptance> getEmployeesByAcceptanceType(Boolean acceptance, int pageNumber, int pageSize) {
        Session session = sessionFactory.openSession();
        List<EmployeeEAcceptance> employees = null;

        try {
            Criteria criteria = session.createCriteria(EmployeeEAcceptance.class);
            criteria.setFirstResult((pageNumber) * pageSize);// check if +1 is needed
            criteria.setMaxResults(pageSize);
            if (!acceptance) {
                criteria.add(Restrictions.ne("acceptance", true));
            } else {
                criteria.add(Restrictions.eq("acceptance", true));
            }

            criteria.addOrder(Order.asc("employeeFirstName"));

            employees = (List<EmployeeEAcceptance>) criteria.list();


        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return employees;
    }

    public EmployeeEAcceptance getEmployeeEAcceptanceByUserName(String userName) {
        Session session = null;
        List<EmployeeEAcceptance> employeeEAcceptanceList;
        try {
            session = sessionFactory.openSession();
            String hql = "from EmployeeEAcceptance where employeeUserName = :employeeUserName";
            Query query = session.createQuery(hql);
            query.setParameter("employeeUserName", userName);


            employeeEAcceptanceList = query.list();
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return employeeEAcceptanceList != null && !employeeEAcceptanceList.isEmpty() ? employeeEAcceptanceList.get(0) : null;
    }
    
    public EmployeeEAcceptance getEmployeeEAcceptanceByEmail(String email) {
        Session session = null;
        List<EmployeeEAcceptance> employeeEAcceptanceList;
        try {
            session = sessionFactory.openSession();
            String hql = "from EmployeeEAcceptance where employeeEmailID = :employeeEmailID";
            Query query = session.createQuery(hql);
            query.setParameter("employeeEmailID", email);


            employeeEAcceptanceList = query.list();
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return employeeEAcceptanceList != null && !employeeEAcceptanceList.isEmpty() ? employeeEAcceptanceList.get(0) : null;
    }

    public List<EmployeeEAcceptance> getEmployeesByAcceptance(boolean acceptance) {
        Session session = null;
        List<EmployeeEAcceptance> employeeEAcceptanceList;
        try {
            session = sessionFactory.openSession();

            String hql = "from EmployeeEAcceptance where acceptance = :acceptance";//add location
            if (!acceptance) {
                hql += " or acceptance is null";
            }
            Query query = session.createQuery(hql);
            query.setParameter("acceptance", acceptance);

            employeeEAcceptanceList = query.list();

        } finally {
            if (session != null) {
                session.close();
            }
        }

        return employeeEAcceptanceList;
    }

    public List<String> getEmployeesEmailAddressByAcceptance(boolean acceptance) {
        Session session = null;
        List<String> employeeEmailList;
        try {
            session = sessionFactory.openSession();
            String hql = "select employeeEmailID from EmployeeEAcceptance where acceptance = :acceptance";//add location
            if (!acceptance) {
                hql += " or acceptance is null";
            }
            Query query = session.createQuery(hql);
            query.setParameter("acceptance", acceptance);

            employeeEmailList = query.list();

        } finally {
            if (session != null) {
                session.close();
            }
        }
        return employeeEmailList;
    }

    public Long getEmployeesCountByAcceptance(Boolean acceptance) {
        Session session = null;
        Long count;
        try {
            session = sessionFactory.openSession();
            String hql = "select count(*) from EmployeeEAcceptance where acceptance = :acceptance";//add location
            if (!acceptance) {
                hql += " or acceptance is null";
            }
            Query query = session.createQuery(hql);
            query.setParameter("acceptance", acceptance);

            count = (Long) query.uniqueResult();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return count;
    }

}
