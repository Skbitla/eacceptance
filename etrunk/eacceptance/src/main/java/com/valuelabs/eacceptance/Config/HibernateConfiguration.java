package com.valuelabs.eacceptance.Config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
public class HibernateConfiguration {

	@Autowired
	private Environment environment;
/*
//	@Bean
//	public DataSource getjdbcDataSource() {
//		BasicDataSource dataSource1 = new BasicDataSource();
//		dataSource1.setDriverClassName(environment
//				.getProperty("jdbc.connection.driver_class"));
//		dataSource1.setUrl(environment
//				.getProperty("database.connection.urlForJdbc"));
//		dataSource1.setUsername(environment
//				.getProperty("jdbc.connection.username"));
//		dataSource1.setPassword(environment
//				.getProperty("jdbc.connection.password"));
//		return dataSource1;
//	}

//	@Bean
//	public JdbcTemplate jdbcTemplate() {
//		System.out.println(getjdbcDataSource());
//		JdbcTemplate jdbcTemplate = new JdbcTemplate();
//		jdbcTemplate.setDataSource(getjdbcDataSource());
//		return jdbcTemplate;
//	}
*/

	@Bean
	public HibernateTemplate hibernateTemplate() {
		HibernateTemplate hibernateTemplate = new HibernateTemplate(
				sessionFactory());
		hibernateTemplate.setCheckWriteOperations(false);
		return hibernateTemplate;
	}

	@Bean
	public SessionFactory sessionFactory() {
		LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
		localSessionFactoryBean.setDataSource(getDataSource());
		localSessionFactoryBean.setPackagesToScan("com.valuelabs.eacceptance");
		localSessionFactoryBean.setHibernateProperties(hibernateProperties());
		// lsfb.setEntityInterceptor(auditInterceptor());

		try {
			localSessionFactoryBean.afterPropertiesSet();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return localSessionFactoryBean.getObject();
	}

	@Bean
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(environment
				.getProperty("hibernate.connection.driver_class"));
		dataSource.setUrl(environment.getProperty("database.connection.url"));
		dataSource.setUsername(environment
				.getProperty("hibernate.connection.username"));
		dataSource.setPassword(environment
				.getProperty("hibernate.connection.password"));
		return dataSource;
	}

	@Bean
	public HibernateTransactionManager hibTransMan() {
		return new HibernateTransactionManager(sessionFactory());
	}

	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", environment.getProperty("hibernate.dialect"));
		// properties.put("hibernate.hbm2ddl.auto", environment.getProperty("hbm2ddl.auto"));
		properties.put("hibernate.show_sql", environment.getProperty("hibernate.show_sql"));
		return properties;
	}

}