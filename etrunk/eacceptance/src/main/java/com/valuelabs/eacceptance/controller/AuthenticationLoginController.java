package com.valuelabs.eacceptance.controller;

import com.valuelabs.eacceptance.dao.EAcceptanceDAO;
import com.valuelabs.eacceptance.model.EmployeeEAcceptance;
import com.valuelabs.eacceptance.model.LoginResponse;
import com.valuelabs.eacceptance.model.UserCredentials;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.servlet.ServletException;
import java.util.Date;
import java.util.Hashtable;

@RestController
@RequestMapping("/auth")
public class AuthenticationLoginController {
	private static final String ADMIN = "admin";
	private static final String USER = "user";
	
	Logger logger = Logger.getLogger(AuthenticationLoginController.class);
	
	private String ldapServer;
	
	@Autowired
	EAcceptanceDAO eAcceptanceDAO;
	
	private String jwtKey;

	@RequestMapping(value = "/loginCredentials", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public LoginResponse login(@RequestBody UserCredentials userCredentials)
			throws ServletException {
		LoginResponse loginResponse = new LoginResponse();
		String username = userCredentials.getUsername();
		String password = userCredentials.getPassword();
		String domain = userCredentials.getDomain();
		String role = USER;
		if (username.equalsIgnoreCase("admin") && password.equals("admin")) {

			loginResponse.setRole(ADMIN);
			role = ADMIN;
		}else if(username.equalsIgnoreCase("user") && password.equals("user")) {
			loginResponse.setRole(USER);
			role = USER;
		}/* else {
			try {
				String email = connectToLdapAndgetEmail(username, password,
						domain);
				if(email == null || email.equals("")){
					throw new IllegalStateException("Invalid user");
				} else {
					EmployeeEAcceptance employeeEAcceptance = eAcceptanceDAO.getEmployeeEAcceptanceByEmail(email);
					if(employeeEAcceptance != null){
						username = employeeEAcceptance.getEmployeeUserName();
						loginResponse.setRole(USER);
						loginResponse.setAccepted(employeeEAcceptance.isAcceptance());
					} else {
						throw new IllegalStateException("Invalid user");
					}
				}
			} catch (Exception e) {
				logger.debug(e);
				e.printStackTrace();
				throw new IllegalStateException("Invalid user");
			}

		}

		String jwtToken = Jwts.builder().setSubject(username)
				.claim("name", username).claim("scope", role)
				.setIssuedAt(new Date())
				.signWith(SignatureAlgorithm.HS256, jwtKey).compact();
		loginResponse.setToken(jwtToken);
		loginResponse.setUsername(username);*/
		return loginResponse;

	}

	private String connectToLdapAndgetEmail(String username, String password,
			String domain) throws Exception {

		final String ldapAdServer = ldapServer;
System.out.println("domain "+domain+" username :" +username+" password "+password );
		final String ldapSearchBase = "DC="+domain+",DC=ads,DC=valuelabs,DC=net";
		final String ldapUsername = domain + "\\" + username;
		final String ldapPassword = password;

		final String ldapAccountToLookup = username;
		Hashtable<String, Object> env = new Hashtable<String, Object>();

		env.put(Context.SECURITY_AUTHENTICATION, "simple");

		if (ldapUsername != null) {
			env.put(Context.SECURITY_PRINCIPAL, ldapUsername);
		}

		if (ldapPassword != null) {
			env.put(Context.SECURITY_CREDENTIALS, ldapPassword);
		}

		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, ldapAdServer);
		env.put(Context.REFERRAL, "follow");

		// ensures that objectSID attribute values

		// will be returned as a byte[] instead of a String

		env.put("java.naming.ldap.attributes.binary", "objectSID");

		// the following is helpful in debugging errors
		// env.put("com.sun.jndi.ldap.trace.ber", System.err);

		DirContext ctx = new InitialLdapContext(env, null);

		// 1) lookup the ldap account
		String email= "";
		try{
			email = findEmailByAccountName(ctx, ldapSearchBase,
					ldapAccountToLookup);
		} finally {
			ctx.close();
		}

		return email;

	}

	public String findEmailByAccountName(DirContext ctx, String ldapSearchBase,
			String accountName) throws Exception {

		String email;

		String searchFilter = "(samaccountname=" + accountName + ")";

		// String searchFilter = "(objectclass=group)";

		SearchControls searchControls = new SearchControls();

		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		searchControls.setTimeLimit(30000);

		String[] attrIDs = { "displayname",

		"mail" };

		searchControls.setReturningAttributes(attrIDs);

		NamingEnumeration<SearchResult> results = ctx.search(
				ldapSearchBase, searchFilter,
				searchControls);

		if (results.hasMoreElements()) {
			Attributes attrs = ((SearchResult) results.next()).getAttributes();
			email = (String) attrs.get("mail").get(0);
		} else {
			throw new Exception("Invalid User");
		}
		ctx.close();
		return email;
	}
	
	@Value("${ldapServer}")
	public void setLdapServer(String ldapServer) {
		this.ldapServer = ldapServer;
	}
	
	@Value("${jwt.secretKey}")
    public void setJwtKey(String jwtKey) {
		this.jwtKey = jwtKey;
	}
}
