package com.valuelabs.eacceptance.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.csvreader.CsvWriter;
import com.valuelabs.eacceptance.dao.EAcceptanceDAO;
import com.valuelabs.eacceptance.model.EmployeeEAcceptance;

@Component
public class CSVUtil {
	
	@Autowired
	EAcceptanceDAO eAcceptanceDAO;
	 private static final Logger logger = Logger.getLogger(PDFGeneratorUtil.class);


	    
	    private String folderPath;
	    
	    @Value("${pdf.folderPath}")
	    public void setFolderPath(String folderPath) {
	        this.folderPath = folderPath;
	    }
	    
	    public File createReport(boolean acceptanceType){
	    	File file = new File(folderPath + "EmployeeAcceptanceReport.csv");
	    	StringBuilder stringBuilder = new StringBuilder();
	    	List<EmployeeEAcceptance> employeeEAcceptances = eAcceptanceDAO.getEmployeesByAcceptance(acceptanceType);
	    	
			try {
				if(file.exists()){
					file.delete();
					file.createNewFile();
				}
				// use FileWriter constructor that specifies open for appending
				CsvWriter csvOutput = new CsvWriter(new FileWriter(file, true), ',');
				
				// if the file didn't already exist then we need to write out the header line
				    csvOutput.write("S.no");
					csvOutput.write("Employee Id");
					csvOutput.write("First name");
					csvOutput.write("Midlle name");
					csvOutput.write("Last name");
					csvOutput.write("Email");
					csvOutput.write("Entity");
					csvOutput.write("Location");
					csvOutput.write("Accepted");
					csvOutput.endRecord();
				// else assume that the file already has the correct header line
				int i=1;
					for(EmployeeEAcceptance employeeEAcceptance: employeeEAcceptances){
						csvOutput.write(i+"");
						csvOutput.write(employeeEAcceptance.getEmployeeId());
						csvOutput.write(employeeEAcceptance.getEmployeeFirstName());
						csvOutput.write(employeeEAcceptance.getEmployeeMiddleName());
						csvOutput.write(employeeEAcceptance.getEmployeeLastName());
						csvOutput.write(employeeEAcceptance.getEmployeeEmailID());
						csvOutput.write(employeeEAcceptance.getEmployeeEntity());
						csvOutput.write(employeeEAcceptance.getEmployeeGeoLocation());
						csvOutput.write(employeeEAcceptance.isAcceptance()? "true" :"false");
						csvOutput.endRecord();
						i++;
					}
				
				
				csvOutput.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	    	
	    	return file;
	    }
	  

}
