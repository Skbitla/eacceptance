package com.valuelabs.eacceptance.model;

/**
 * Created by SRINIVAS on 8/20/2017.
 */
public class LoginResponse {
    private String role;
    private String token;
    private String username;
    private Boolean accepted;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

	public String getUsername() {
		return username;
	}

	public Boolean isAccepted() {
		return accepted;
	}

	public void setAccepted(Boolean accepted) {
		this.accepted = accepted;
	}

	public void setUsername(String username) {
		this.username = username;
	}
    
    
}
