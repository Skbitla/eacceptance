package com.valuelabs.eacceptance.model;

public class EmployeeAcceptanceCount {
	
	long acceptedEmployeesCount;
	long noActionEmployeesCount;
	
	
	public long getAcceptedEmployeesCount() {
		return acceptedEmployeesCount;
	}
	public void setAcceptedEmployeesCount(long acceptedEmployeesCount) {
		this.acceptedEmployeesCount = acceptedEmployeesCount;
	}
	public long getNoActionEmployeesCount() {
		return noActionEmployeesCount;
	}
	public void setNoActionEmployeesCount(long noActionEmployeesCount) {
		this.noActionEmployeesCount = noActionEmployeesCount;
	}
	
	

}
