app.controller('HomeController', function(EmployeeService, $scope, $rootScope,
		$stateParams, $state, LoginService, $http) {

	// $scope.truefalse = true;

	$scope.user = LoginService.getUserName();
	$scope.accepted = false;
	// get EMployee Data

	if ($scope.user) {
		var employeeInfo = EmployeeService.getEmployeeData();
		employeeInfo.then(function(response) {
			$scope.employeeData = response.data;
		});
	} else {
		$state.go('login');
	}

	$scope.logout = function($window) {
		$('.modal').modal('hide');
		$('.modal-backdrop.fade.in').remove();
		$('body').removeClass('modal-open');
		LoginService.logoutUser();
		$state.go('login');

	};
	$scope.saveUserPreference = function(eAcceptance) {
		
		var user = LoginService.getUserName();
		var url = 'eAccept/update/' + user + '/' + eAcceptance;
		if($scope.accepted){
			LoginService.logoutUser();
			$state.go('login');
		} else{
			$scope.accepted=true;
			$http.get(url).success(function(data, status, headers, config) {
				console.log("Success while saving user preference");
				$('.modal').modal('show');
//				$state.go('login');
			}).error(function(err) {
				console.log("Error while saving user preference");
				LoginService.logoutUser();
				$state.go('login');
			});	
		}
		
	}
});