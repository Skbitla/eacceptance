
app.factory('LoginService', function() {
	
//	var admin = 'admin';
//	var admin2 = 'superadmin';
//	var pass = 'password';
//	var pass2 = 'password';

	var isAuthenticated = false;
	var user = {};
	return {
//		login : function(username, password) {
//
//			isAuthenticated = username === admin2 || password === pass;
//
//			return isAuthenticated;
//		},
//		isAuthenticated : function() {
//			return isAuthenticated;
//		},
		getUserName: function() {
			return localStorage.getItem("userName");
		},
		setUserName: function(userName) {
			localStorage.setItem("userName", userName);
			//user.name = userName;
		},
	    getToken: function() {
        	return localStorage.getItem("token");
        },
        setToken: function(token) {
        			localStorage.setItem("token", token);
        		},
		logoutUser: function() {
			localStorage.removeItem('userName');
			localStorage.removeItem('token');
		}
		
		
	};

});

app.factory('EmployeeService', ['$http', '$rootScope', 'LoginService', function($http, $rootScope, LoginService) {

	return {
		getEmployeeData : function() {
									
			/*var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();

			if(dd<10) {
			    dd = '0'+dd
			} 

			if(mm<10) {
			    mm = '0'+mm
			} 

			today = dd + ' ' + MMM + ',' + yyyy;*/	//07 Aug, 2017	
			
			return $http.get("eAccept/"+LoginService.getUserName())
	            .success(function(data, status, headers, config)
	            {
            	var today = new Date();
	    			var dd = today.getDate();
	    			var mm = today.getMonth()+1;
	    			var yyyy = today.getFullYear();

	    			/*if(dd<10) {
	    			    dd = '0'+dd
	    			} 

	    			if(mm<10) {
	    			    mm = '0'+mm
	    			} */

	    			//today = mm + '/' + dd + '/' + yyyy;	
	            	
	            	
	            	
	            	var monthNames = ["January", "February", "March", "April", "May", "June",
	            	                  "July", "August", "September", "October", "November", "December"
	            	                ];

	            	                var d = new Date();
	            	                
	            	                today = dd + ' '+' '+monthNames[d.getMonth()]+', '+yyyy;
	            	                //document.write("The current month is " + monthNames[d.getMonth()]);
	            	               
	            	                
	            	data.todayDate = today;
	            	return data;
	            }).error(function(err) {
	            	console.log("Error occurred while fetching emp Data");
	            });
				}
	};

}]);


app.factory('HRService', ['$http', '$rootScope', function($http, $rootScope) {

	return {
		getEmployeeCounts : function() {

			return $http.get("eAccept/counts")
	            .success(function(data, status, headers, config)
	            {
            		return data;
	            }).error(function(err) {
	            	console.log("Error occurred while fetching HR counts");
	            });
				}
	};

}]);

app.factory('sessionInjector', ['LoginService', function(LoginService) {
        var sessionInjector = {
            request: function(config) {
//                if (LoginService.getUserName()) {
                    config.headers['Authorization'] = LoginService.getToken();
//                }
                return config;
            }
        };
        return sessionInjector;
    }]);
    app.config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push('sessionInjector');
    }]);





















