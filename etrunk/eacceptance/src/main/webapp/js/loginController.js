app.controller('LoginController', function($scope, $rootScope, $stateParams,
		$state, LoginService, $http) {
	$rootScope.title = "AngularJS Login Sample";

	$scope.formSubmit = function() {

		var username = $scope.username;
		var password = $scope.password;
		var domain = $scope.domain;

		$.ajax({
			type : "POST",
			url : "auth/loginCredentials",
			dataType : "json",
			contentType : 'application/json',
			data : JSON.stringify({
				"username" : username,
				"password" : password,
				"domain" : domain
			}),
			async : false,

			success : function(data, status) {

				if (data.role == "admin") {
					$state.go('hrScreen');
					LoginService.setToken(data.token);
					$rootScope.user = data.username;
					LoginService.setUserName(data.username);
				} else if (data.role == "user") {
					if (data.accepted) {
						$state.go('login');
						$scope.error = "You have already accepted to the terms";
					} else {
						$state.go('home');
						$rootScope.user = data.username;
						LoginService.setUserName(data.username);
						LoginService.setToken(data.token);
					}

				} else {
					$scope.error = '';
					$scope.username = '';
					$scope.password = '';
					$scope.error = "Incorrect username/password !";
					$rootScope.user = '';
					LoginService.setUserName('');

				}

			},
			error : function(e, status) {
				// alert("error status");
				$scope.error = "Incorrect username/password !";
			}

		});

	};

});
