app.controller('HrController', function($scope, $rootScope, $stateParams,
		$state, LoginService, $http, HRService) {
	// $rootScope.title = "AngularJS Login Sample";

	$scope.user = LoginService.getUserName();
	$scope.users = [];
	$scope.IsVisible = true;
	$scope.showAccepted = false;
	$scope.showNotAccepted = false;
	// get EMployee Data

	if ($scope.user) {
		var employeeCounts = HRService.getEmployeeCounts();
		employeeCounts.then(function(response) {
			$scope.employeeCounts = response.data;
		});
	} else {
		$state.go('login');
	}

	$scope.acceptedUsers = [];
	$scope.notAcceptedUsers = [];
	$http.get("eAccept/list/true").success(function(response) {
		$scope.acceptedUsers = response;
	});

	$http.get("eAccept/list/false").success(function(response) {
		$scope.notAcceptedUsers = response
	});

	$scope.sort = function(keyname) {
		$scope.sortKey = keyname; // set the sortKey to the param passed
		$scope.reverse = !$scope.reverse; // if true make it false and vice
											// versa
	}

	$scope.showEmployees = function(eAcceptance) {
		if (eAcceptance) {
			$state.go('acceptedEmployees');
		} else {
			$state.go('notAcceptedEmployees');
		}
	}

	$scope.goBack = function() {
		$state.go('hrScreen');
	}
	
	$scope.sendEmailToNotAccepted = function() {
//		$http.get("eAccept/sendEmailToNotAccepted").success(function(response) {
//			$state.go('hrScreen');
//		});
	}
	$scope.logout = function($window) {
		LoginService.logoutUser();
		$state.go('login');
	};
});

