var app = angular.module('myApp', ['ui.router','angularUtils.directives.dirPagination']);
app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/login');

//    app.config(['$httpProvider', function ($httpProvider) {
//        $httpProvider.defaults.headers.common = {
//            'Authorization': 'Basic d2VudHdvcnRobWFuOkNoYW5nZV9tZQ==',
//            'Accept': 'application/json;odata=verbose'
//          };
//    }]);


    $stateProvider
      .state('login', {
        url : '/login',
        templateUrl : 'html/loginPage.html',
        controller : 'LoginController'
      })
      .state('home', {
        url : '/home',
        templateUrl : 'html/HomeScreen.html',
        controller : 'HomeController'
      })
//      .state('projectSelection', {
//        url : '/projectSelection',
//        templateUrl : '/html/ProjectManager.html',
//        controller : 'ProjectManagerController'
//      })
       .state('hrScreen', {
        url : '/hrScreen',
        templateUrl : 'html/superAdmin.html',
        controller : 'HrController'
      })
       .state('acceptedEmployees', {
        url : '/acceptedEmployees',
        templateUrl : 'html/acceptedEmployees.html',
        controller : 'HrController'
      })
       .state('notAcceptedEmployees', {
        url : '/notAcceptedEmployees',
        templateUrl : 'html/notAcceptedEmployees.html',
        controller : 'HrController'
      })
       
      
  }]);